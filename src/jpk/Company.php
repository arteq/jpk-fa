<?php
/**
 * Class to represent company/client entity 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\JPK_FA;

class Company
{
	/**
	 * List of valid fields
	 * @var array
	 */ 
	private $fields = ['NIP', 'PelnaNazwa', 'REGON', 'KodKraju', 'Wojewodztwo', 'Powiat', 'Gmina', 'Ulica', 'NrDomu', 'NrLokalu', 'Miejscowosc', 'KodPocztowy', 'Poczta', 'Adres'];

	/**
	 * Object fields as key->value array
	 * @var array
	 */ 
	private $data = [];

	/* ====================================================================== */
	
	/**
	 * Create new company
	 * 
	 * @param string $name
	 * @param string $nip
	 * @param string $address
	 * @param string $countryCode
	 */ 
	public function __construct($name, $nip, $address = '', $countryCode = 'PL')
	{
		// set empty values for all fields
		$this->data = array_fill_keys($this->fields, '');

		$this->set('PelnaNazwa', $name);
		$this->set('KodKraju', $countryCode);
		$this->set('NIP', $nip);		
		$this->set('Adres', $address);

		// check for valid values
		if (empty($this->data['PelnaNazwa']))
			throw new JpkException('Nazwa firmy nie może być pusta');
		
		if ($countryCode == 'PL' && preg_match('/^\d{10}$/', $this->data['NIP']) != 1)
			throw new JpkException('Nieprawidłowy numer NIP ['.$this->data['NIP'].']');
	}

	/* ====================================================================== */
	
	/**
	 * Set field value, perform optional value sanitization
	 * 
	 * @param string $field
	 * @param mixed $value
	 */ 
	public function set($field, $value)
	{
		if (method_exists($this, 'sanitize'.$field))
			$value = call_user_func([$this, 'sanitize'.$field], $value);

		if (in_array($field, $this->fields))
			$this->data[$field] = $value;
	}

	/* ====================================================================== */
	
	/**
	 * Get field value, or empty string if field does not exist
	 * 
	 * @param string $field
	 * @return mixed
	 */ 
	public function get($field)
	{
		$result = '';
		if (method_exists($this, 'get'.$field))
			return call_user_func([$this, 'get'.$field]);

		return isset($this->data[$field]) ? $this->data[$field] : '';
	}

	/* ====================================================================== */
	
	/**
	 * Get parsed company full address. If 'Adres' field is empty then create it by 
	 * joining fields 'Miejscowosc', 'Ulica', 'NrDomu', ... 
	 * 
	 * @return string
	 */ 
	public function getAdres()
	{
		if (empty($this->data['Adres']))
		{
			$address = '';
			if (!empty($this->data['Miejscowosc']) && $this->data['Miejscowosc'] != $this->data['Poczta'])
				$address .= trim($this->data['Miejscowosc'])." ";

			if (!empty($this->data['Ulica']))
				$address .= trim($this->data['Ulica'])." ";
			if (!empty($this->data['NrDomu']))
				$address .= trim($this->data['NrDomu']);
			if (!empty($this->data['NrLokalu']))
				$address .= "/".trim($this->data['NrLokalu']);

			if (!empty($this->data['KodPocztowy']))
				$address .= ", ".trim($this->data['KodPocztowy']);
			if (!empty($this->data['Poczta']))
				$address .= " ".trim($this->data['Poczta']);

			$this->set('Adres', $address);
		}

		return $this->data['Adres'];
	}

	/* ====================================================================== */

	/**
	 * Return sanitized Adres, change new lines to spaces
	 * 
	 * @param string $address
	 * @return string
	 */ 
	private function sanitizeAdres($address)
	{
		$address = str_replace(["\r\n", "\n", "\r"], ' ', $address);
		return trim($address);
	}

	/* ====================================================================== */
	
	/**
	 * Return sanitized NIP (only digits, no delimiters), only for PL companies.
	 * For foreign companies return as is.
	 * 
	 * @param string $nip
	 * @return string
	 */ 
	private function sanitizeNIP($nip)
	{
		if ($this->data['KodKraju'] != 'PL')
			return $nip;

		// \D = non-digit character
		$nip = preg_replace('/\D/', '', $nip);
		return trim($nip);
	}

	/* ====================================================================== */
	
	/**
	 * Return sanitized REGON (only digits, no delimiters)
	 * 
	 * @param string $regon
	 * @return string
	 */ 
	private function sanitizeREGON($regon)
	{
		// \D = non-digit character
		$regon = preg_replace('/\D/', '', $regon);
		return trim($regon);
	}
}