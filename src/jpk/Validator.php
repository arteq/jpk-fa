<?php
/**
 * JPK_FA report validator
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\JPK_FA;

class Validator
{
	const SCHEMA = __DIR__.'/../../xsd/Schemat_JPK_FA_v1-0.xsd';
	const NS_JPK = 'http://jpk.mf.gov.pl/wzor/2016/03/09/03095/';
	const NS_TYPES = 'http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/';

	private $dom;
	private $xp;
	private $isValid = true;
	private $errors = [];

	/* ====================================================================== */
	
	/**
	 * Create validator object and validates report
	 * 
	 * @param string $report XML document
	 */ 
	public function __construct($report)
	{
		libxml_use_internal_errors(true);

		$dom = new \DOMDocument();
		$dom->loadXML($report);

		$xp = new \DOMXPath($dom);
		$xp->registerNamespace("tns", self::NS_JPK);
		$xp->registerNamespace("etd", self::NS_TYPES);
		$this->xp = $xp;

		$this->dom = $dom;
		$this->validateSchema();
		$this->validateInvoicesCnt();
		$this->validateInvoicesRowsCnt();
		$this->validateInvoicesNet();
		$this->validateInvoicesRowsNet();
		$this->validateDates();
		$this->validateRows();
		$this->validateUniqueInvoiceNr();
		$this->validateExcemptLegalBasis();
	}
	
	/* ====================================================================== */
	
	public function isValid()
	{
		return $this->isValid;
	}

	/* ====================================================================== */
	
	public function getErrors()
	{
		return $this->errors;
	}

	/* ====================================================================== */

	/**
	 * Check if is valid XSD schema
	 */ 
	private function validateSchema()
	{
		if (!$this->dom->schemaValidate(self::SCHEMA)) 
			$this->isValid = false;

		$errors = (array)libxml_get_errors();
		foreach ($errors as $error) 
		{
			$this->errors[] = "Error [code: ".$error->code.", line: ".$error->line."] ".trim($error->message);
		}
	}

	/* ====================================================================== */
	
	/**
	 * Check if Faktura count match FakturaCtrl/LiczbaFaktur
	 */ 
	private function validateInvoicesCnt()
	{
		$invoicesCtrl = $this->xp->query('//tns:FakturaCtrl/tns:LiczbaFaktur')->item(0)->nodeValue;
		$invoicesCnt =	$this->xp->query('//tns:Faktura')->length;

		if ($invoicesCnt != $invoicesCtrl)
		{
			$this->isValid = false;
			$this->errors[] = 'Liczba faktur ['.$invoicesCnt.'] nie zgadza się z liczbą kontrolną ['.$invoicesCtrl.']';
		}
	}

	/* ====================================================================== */
	
	/**
	 * Check if FakturaWiersz count match FakturaWierszCtrl/LiczbaWierszyFaktur
	 */ 
	private function validateInvoicesRowsCnt()
	{
		$invoicesCtrl = $this->xp->query('//tns:FakturaWierszCtrl/tns:LiczbaWierszyFaktur')->item(0)->nodeValue;
		$invoicesCnt =	$this->xp->query('//tns:FakturaWiersz')->length;

		if ($invoicesCnt != $invoicesCtrl)
		{
			$this->isValid = false;
			$this->errors[] = 'Liczba wierszy faktur ['.$invoicesCnt.'] nie zgadza się z liczbą kontrolną ['.$invoicesCtrl.']';
		}
	}

	/* ====================================================================== */
	
	/**
	 * Check if Report DataOd if before DataDo
	 * Check if each Invoice issueDate is within Report date range
	 */ 
	private function validateDates()
	{
		$reportStart = $this->xp->query('//tns:Naglowek/tns:DataOd')->item(0)->nodeValue;
		$reportEnd = $this->xp->query('//tns:Naglowek/tns:DataDo')->item(0)->nodeValue;

		if ($reportStart > $reportEnd)
		{
			$this->isValid = false;
			$this->errors[] = 'Nieprawidłowy zakres dat raportu';
		}

		$invoices = $this->xp->query('//tns:Faktura');
		foreach ($invoices as $invoice)
		{
			$dateIssue = $this->xp->query('tns:P_1', $invoice)->item(0)->nodeValue;
			if ($dateIssue < $reportStart || $dateIssue > $reportEnd)
			{
				$this->isValid = false;
				$this->errors[] = 'Data wystawienia ['.$dateIssue.'] faktury nr ['.$this->xp->query('tns:P_2A', $invoice)->item(0)->nodeValue.'] poza zakresem dat raportu';
			}
		}
	}

	/* ====================================================================== */
	
	/**
	 * Check if invoices netto sum is equal to controll row
	 */ 
	private function validateInvoicesNet()
	{
		$sum = 0;

		// get control value
		$nettoInvoicesCtrl = $this->xp->query('//tns:FakturaCtrl/tns:WartoscFaktur')->item(0)->nodeValue;

		// get sum of all Invoices
		$nettoInvoices = $this->xp->query('//tns:Faktura/tns:P_13_1
			| //tns:Faktura/tns:P_13_2 
			| //tns:Faktura/tns:P_13_3 
			| //tns:Faktura/tns:P_13_4 
			| //tns:Faktura/tns:P_13_5 
			| //tns:Faktura/tns:P_13_6 
			| //tns:Faktura/tns:P_13_7 ');

		foreach ($nettoInvoices as $netto)
		{
			$sum += $netto->nodeValue;
		}
		
		// float compare to 3 decimal places
		if (bccomp($sum, $nettoInvoicesCtrl, 3))
		{
			$this->isValid = false;
			$this->errors[] = 'Suma wartości netto faktur ['.sprintf('%.2f', $sum).'] nie zgadza się z sumą kontrolną ['.sprintf('%.2f', $nettoInvoicesCtrl).']';
		}
	}

	/* ====================================================================== */
	
	/**
	 * Check if invoices rows netto sum is equal to controll row
	 */ 
	private function validateInvoicesRowsNet()
	{
		$sum = 0;

		// get control value
		$nettoInvoicesCtrl = $this->xp->query('//tns:FakturaWierszCtrl/tns:WartoscWierszyFaktur')->item(0)->nodeValue;

		// get sum of all Invoices
		$nettoInvoices = $this->xp->query('//tns:FakturaWiersz/tns:P_11');

		foreach ($nettoInvoices as $netto)
		{
			$sum += $netto->nodeValue;
		}

		// float compare to 3 decimal places
		if (bccomp($sum, $nettoInvoicesCtrl, 3))
		{
			$this->isValid = false;
			$this->errors[] = 'Suma wartości netto wierszy faktur ['.sprintf('%.2f', $sum).'] nie zgadza się z sumą kontrolną ['.sprintf('%.2f', $nettoInvoicesCtrl).']';
		}
	}

	/* ====================================================================== */
	
	/**
	 * Check if each Invoice has > 0 InvoiceRow
	 * Check if each InvoiceRow belongs to existing Invoice
	 */ 
	private function validateRows()
	{
		$invoices = $this->xp->evaluate('//tns:Faktura/tns:P_2A');
		$rows = $this->xp->query('//tns:FakturaWiersz/tns:P_2B');

		$invoicesNr = [];
		$invoicesRowsNr = [];

		foreach ($invoices as $invoiceNr)
		{
			$invoicesNr[] = $invoiceNr->nodeValue;
		}

		foreach ($rows as $row)
		{
			$invoicesRowsNr[] = $row->nodeValue;

			// each invoice row belongs to existing invoice
			if (!in_array($row->nodeValue, $invoicesNr))
			{
				$this->isValid = false;
				$this->errors[] = 'Pozycja faktury ['.$row->nodeValue.'] nie ma odpowiednika w fakturach';
			}
		}
		
		// each invoice has > 0 rows
		foreach ($invoicesNr as $invoiceNr)
		{
			if (!in_array($invoiceNr, $invoicesRowsNr))
			{
				$this->isValid = false;
				$this->errors[] = 'Faktura ['.$invoiceNr.'] nie ma żadnych pozycji';
			}
		}
	}

	/* ====================================================================== */
	
	/**
	 * Check if each Invoice nr is unique
	 */ 
	private function validateUniqueInvoiceNr()
	{
		$invoices = $this->xp->evaluate('//tns:Faktura/tns:P_2A');
		$invoicesNr = [];

		foreach ($invoices as $invoiceNr)
		{
			// check if is unique
			if (in_array($invoiceNr->nodeValue, $invoicesNr))
			{
				$this->isValid = false;
				$this->errors[] = 'Numer faktury ['.$invoiceNr->nodeValue.'] nie jest unikalny';
			}

			$invoicesNr[] = $invoiceNr->nodeValue;
		}
	}

	/* ====================================================================== */
	
	private function validateCorrections()
	{
		// existing invoice
		// has reason
	}

	/* ====================================================================== */
	
	/**
	 * Check if each Invoice with isExcempt set has legal basis provided
	 */ 
	private function validateExcemptLegalBasis()
	{
		// get invoices with VAT exempt
		$invoicesWithExempt = $this->xp->query('//tns:Faktura/tns:P_19[. = "true"]');
		
		foreach ($invoicesWithExempt as $invoice)
		{
			$legalBasis = $this->xp->evaluate('//tns:P_19A | //tns:P_19B | //tns:P_19C', $invoice->parentNode);

			// need at least one legal basis provided to use vat exempt rate
			if ($legalBasis->length === 0)
			{
				$invoiceNr = $this->xp->evaluate('//tns:P_2A', $invoice->parentNode);

				$this->isValid = false;
				$this->errors[] = 'Faktura ['.$invoiceNr->item(0)->nodeValue.'] nie ma podanej podstawy prawnej do zwolnienia VAT';
			}
		}
	}
}