<?php
/**
 * Class to represent company/client entity 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\JPK_FA;

class JpkException extends \Exception
{
}