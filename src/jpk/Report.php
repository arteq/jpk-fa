<?php
/**
 * 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\JPK_FA;

class Report
{
	const NS_JPK = 'http://jpk.mf.gov.pl/wzor/2016/03/09/03095/';
	const NS_TYPES = 'http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/';

	const KOD_SYSTEMOWY = 'JPK_FA (1)';
	const WERJSA_SCHEMY = '1-0';

	const EU_COUNTRIES = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'EL', 'HR', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'GB', 'IC', 'XI', 'XJ', 'MC'];


	protected $dateStart;
	protected $dateEnd;
	protected $currency;
	protected $officeCode;

	/**
	 * Report object
	 * @var \DOMDocument
	 */ 
	protected $report;

	/**
	 * Company
	 * @var Company
	 */ 
	protected $company;

	/**
	 * Array of Invoice
	 * @var array
	 */ 
	private $invoices = [];

	/**
	 * Array of InvoiceRow
	 * @var array
	 */ 
	private $invoicesRows = [];

	/**
	 * List of valid tax rates
	 * @var array
	 */ 
	private $taxRates = [0.23, 0.08, 0.05, 0.0, 0.0];

	/**
	 * Total netto amount from invoices
	 * @var float
	 */ 
	private $totalNet = 0.0;

	/**
	 * Total netto amount from invoices rows
	 * @var float
	 */ 
	private $totalNetRows = 0.0;

	/* ====================================================================== */
	
	/**
	 * Create new JPK-FA report
	 * 
	 * @param string $dateStart
	 * @param string $dateEnd
	 * @param string $officeCode
	 * @param string $currency
	 */ 
	public function __construct($dateStart, $dateEnd, $officeCode, $currency = 'PLN')
	{
		$this->dateStart = $dateStart;
		$this->dateEnd = $dateEnd;
		$this->currency = $currency;
		$this->officeCode = $officeCode;

		$report = new \DOMDocument("1.0", "utf-8");
		$report->formatOutput = true;

		$this->report = $report;
	}

	/* ====================================================================== */
	
	/**
	 * Add new invoice to report
	 * 
	 * @param Invoice $invoice
	 */ 
	public function addInvoice(Invoice $invoice)
	{
		$this->invoices[] = $invoice;
		$this->totalNet += $invoice->getTotalNet();

		$invoiceRows = $invoice->getRows();
		foreach ($invoiceRows as $invoiceRow)
		{
			$this->invoicesRows[] = $invoiceRow;
			$this->totalNetRows += $invoiceRow->priceNet;
		}
	}

	/* ====================================================================== */
	
	/**
	 * Set company data
	 * 
	 * @param Company $company
	 */ 
	public function setCompany(Company $company)
	{
		$this->company = $company;
	}

	/* ====================================================================== */
	
	/**
	 * Set non-default tax rates (ints)
	 * 
	 * @param int $rate1
	 * @param int $rate2
	 * @param int $rate3
	 * @param int $rate4
	 * @param int $rate5
	 */ 
	public function setTaxRates($rate1, $rate2, $rate3, $rate4, $rate5)
	{
		$this->taxRates = [$rate1, $rate2, $rate3, $rate4, $rate5];
	}

	/* ====================================================================== */
	
	/**
	 * Generate JPK-FA report as XML
	 * 
	 * @return string
	 */ 
	public function generate()
	{
		// root element
		$jpk = $this->report->createElementNS(self::NS_JPK, 'JPK');
		$jpk->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:etd', self::NS_TYPES);
		$this->report->appendChild($jpk);

		// nagłówek
		$jpk->appendChild($this->getHeader());

		// firma
		$jpk->appendChild($this->getCompany());

		// faktury
		foreach ($this->getInvoices() as $invoice)
		{
			$jpk->appendChild($invoice);
		}

		// faktury - suma kontrolna
		$jpk->appendChild($this->getInvoicesCtrl());

		// stawki podatku
		$jpk->appendChild($this->getTaxRates());

		// faktury wiersze
		foreach ($this->getInvoicesRows() as $invoiceRow)
		{
			$jpk->appendChild($invoiceRow);
		}

		// faktury wiersze - suma kontrolna
		$jpk->appendChild($this->getInvoicesRowsCtrl());		

		return $this->report->saveXML();
	}

	/* ====================================================================== */
	
	/**
	 * Create JPK header
	 *
	 * @return \DOMElement
	 */  
	private function getHeader()
	{
		$naglowek = $this->report->createElement('Naglowek');

		// kodFormularza
		$kodFormularza = $this->report->createElement('KodFormularza', 'JPK_FA');

		$attr1 = $this->report->createAttribute('kodSystemowy');
		$attr1->value = self::KOD_SYSTEMOWY;
		$kodFormularza->appendChild($attr1);

		$attr2 = $this->report->createAttribute('wersjaSchemy');
		$attr2->value = self::WERJSA_SCHEMY;
		$kodFormularza->appendChild($attr2);

		$naglowek->appendChild($kodFormularza);

		$wariantFormularza = $this->report->createElement('WariantFormularza', '1');
		$naglowek->appendChild($wariantFormularza);

		$celZlozenia = $this->report->createElement('CelZlozenia', '1');
		$naglowek->appendChild($celZlozenia);

		$now = new \DateTime();
		$dataWytworzenia = $this->report->createElement('DataWytworzeniaJPK', $now->format(\DateTime::ATOM));
		$naglowek->appendChild($dataWytworzenia);

		$dataOd = $this->report->createElement('DataOd', $this->dateStart);
		$naglowek->appendChild($dataOd);

		$dataDo = $this->report->createElement('DataDo', $this->dateEnd);
		$naglowek->appendChild($dataDo);

		$domyslnyKodWaluty = $this->report->createElement('DomyslnyKodWaluty', $this->currency);
		$naglowek->appendChild($domyslnyKodWaluty);

		$kodUrzedu = $this->report->createElement('KodUrzedu', $this->officeCode);
		$naglowek->appendChild($kodUrzedu);

		return $naglowek;
	}

	/* ====================================================================== */
	
	/**
	 * Create Company header
	 * 
	 * @return \DOMElement
	 */ 
	private function getCompany()
	{
		$fieldsIdentyfikator = ['NIP', 'PelnaNazwa', 'REGON'];
		$fieldsAdres = ['KodKraju', 'Wojewodztwo', 'Powiat', 'Gmina', 'Ulica', 'NrDomu', 'NrLokalu', 'Miejscowosc', 'KodPocztowy', 'Poczta'];

		$podmiot1 = $this->report->createElement('Podmiot1');

		$identyfikatorPodmiotu = $this->report->createElement('IdentyfikatorPodmiotu');
		foreach ($fieldsIdentyfikator as $field)
		{
			$element = $this->report->createElement('etd:'.$field, $this->company->get($field));
			$identyfikatorPodmiotu->appendChild($element);
		}
		$podmiot1->appendChild($identyfikatorPodmiotu);

		$adresPodmiotu = $this->report->createElement('AdresPodmiotu');
		foreach ($fieldsAdres as $field)
		{
			$value = $this->company->get($field);
			
			// skip for empty values, ex no 'NrLokalu'
			if (empty($value))
				continue;

			$element = $this->report->createElement('etd:'.$field, $value);
			$adresPodmiotu->appendChild($element);
		}
		$podmiot1->appendChild($adresPodmiotu);

		return $podmiot1;
	}

	/* ====================================================================== */
	
	/**
	 * Create tax rates
	 * 
	 * @return \DOMElement
	 */ 
	private function getTaxRates()
	{
		$stawkiPodatku = $this->report->createElement('StawkiPodatku');

		$n = 1;
		foreach ($this->taxRates as $rate)
		{
			$stawka = $this->report->createElement('Stawka'.$n++, $rate);
			$stawkiPodatku->appendChild($stawka);
		}

		return $stawkiPodatku;
	}

	/* ====================================================================== */
	
	/**
	 * Create invoices
	 * 
	 * @return array
	 */ 
	private function getInvoices()
	{
		$invoices = [];

		foreach ($this->invoices as $invoice)
		{
			$faktura = $this->report->createElement('Faktura');
			$faktura->setAttribute('typ', 'G');

			// data wystawienia
			$p_1 = $this->report->createElement('P_1', $invoice->dateIssue);
			$faktura->appendChild($p_1);

			// numer kolejny
			$p_2a = $this->report->createElement('P_2A', $invoice->nr);
			$faktura->appendChild($p_2a);

			// nazwa nabywcy
			$p_3a = $this->report->createElement('P_3A', htmlspecialchars($invoice->client->get('PelnaNazwa')));
			$faktura->appendChild($p_3a);

			// adres nabywcy
			$p_3b = $this->report->createElement('P_3B', htmlspecialchars($invoice->client->get('Adres')));
			$faktura->appendChild($p_3b);

			// nazwa sprzedawcy
			$p_3c = $this->report->createElement('P_3C', htmlspecialchars($this->company->get('PelnaNazwa')));
			$faktura->appendChild($p_3c);

			// adres sprzedawcy
			$p_3d = $this->report->createElement('P_3D', htmlspecialchars($this->company->get('Adres')));
			$faktura->appendChild($p_3d);

			// kod VAT EU sprzedawcy
			$p_4a = $this->report->createElement('P_4A', $this->company->get('KodKraju'));
			$faktura->appendChild($p_4a);

			// NIP sprzedawcy
			$p_4b = $this->report->createElement('P_4B', $this->company->get('NIP'));
			$faktura->appendChild($p_4b);

			// kod VAT EU nabywcy, tylko dla europejskich kontrahentów
			if (in_array($invoice->client->get('KodKraju'), self::EU_COUNTRIES))
			{
				$p_5a = $this->report->createElement('P_5A', $invoice->client->get('KodKraju'));
				$faktura->appendChild($p_5a);
			}
			
			// NIP nabywcy, tylko dla polskich kontrahentów
			if ($invoice->client->get('KodKraju') == 'PL')
			{
				$p_5b = $this->report->createElement('P_5B', $invoice->client->get('NIP'));
				$faktura->appendChild($p_5b);
			}

			// data wykonania usługi
			$p_6 = $this->report->createElement('P_6', $invoice->dateSell);
			$faktura->appendChild($p_6);

			// kwota netto 23%
			$p_13_1 = $this->report->createElement('P_13_1', sprintf("%.2f", $invoice->getTotalNet_23()) );
			$faktura->appendChild($p_13_1);

			// podatek od netto 23%
			$p_14_1 = $this->report->createElement('P_14_1', sprintf("%.2f", $invoice->getTotalNet_23() * $this->taxRates[0]) );
			$faktura->appendChild($p_14_1);

			// kwota netto 8%
			$p_13_2 = $this->report->createElement('P_13_2', sprintf("%.2f", $invoice->getTotalNet_8()) );
			$faktura->appendChild($p_13_2);

			// podatek od netto 8%
			$p_14_2 = $this->report->createElement('P_14_2', sprintf("%.2f", $invoice->getTotalNet_8() * $this->taxRates[1]) );
			$faktura->appendChild($p_14_2);

			// kwota netto 5%
			$p_13_3 = $this->report->createElement('P_13_3', sprintf("%.2f", $invoice->getTotalNet_5()) );
			$faktura->appendChild($p_13_3);

			// podatek od netto 5%
			$p_14_3 = $this->report->createElement('P_14_3', sprintf("%.2f", $invoice->getTotalNet_5() * $this->taxRates[2]) );
			$faktura->appendChild($p_14_3);

			// kwota netto 0%
			$p_13_6 = $this->report->createElement('P_13_6', sprintf("%.2f", $invoice->getTotalNet_0()) );
			$faktura->appendChild($p_13_6);

			// kwota netto zw
			$p_13_7 = $this->report->createElement('P_13_7', sprintf("%.2f", $invoice->getTotalNet_zw()) );
			$faktura->appendChild($p_13_7);

			// kwota brutto
			$p_15 = $this->report->createElement('P_15', sprintf("%.2f", $invoice->getTotalGross()) );
			$faktura->appendChild($p_15);

			// metoda kasowa
			$p_16 = $this->report->createElement('P_16', 'false');
			$faktura->appendChild($p_16);

			// samofakturowanie
			$p_17 = $this->report->createElement('P_17', 'false');
			$faktura->appendChild($p_17);

			// reverse-charge
			$p_18 = $this->report->createElement('P_18', ($invoice->isReverseCharge()) ? 'true' : 'false');
			$faktura->appendChild($p_18);

			// zwolnione
			$p_19 = $this->report->createElement('P_19', ($invoice->isExcempt()) ? 'true' : 'false');
			$faktura->appendChild($p_19);

			if ($invoice->isExcempt())
			{
				if (!empty($invoice->excemptLegalBasis['P_19A']))
				{
					$p_19a = $this->report->createElement('P_19A', $invoice->excemptLegalBasis['P_19A']);
					$faktura->appendChild($p_19a);
				}

				if (!empty($invoice->excemptLegalBasis['P_19B']))
				{
					$p_19b = $this->report->createElement('P_19B', $invoice->excemptLegalBasis['P_19B']);
					$faktura->appendChild($p_19b);
				}

				if (!empty($invoice->excemptLegalBasis['P_19C']))
				{
					$p_19c = $this->report->createElement('P_19C', $invoice->excemptLegalBasis['P_19C']);
					$faktura->appendChild($p_19c);
				}
			}

			// podatek od dostaw
			$p_20 = $this->report->createElement('P_20', 'false');
			$faktura->appendChild($p_20);

			// FV wystawiona przez przdstawiciela podatkowego
			$p_21 = $this->report->createElement('P_21', 'false');
			$faktura->appendChild($p_21);

			// FV wystawiona przez drugiego podatnika, art. 135 ust. 1 pkt 4 lit. b,c
			$p_23 = $this->report->createElement('P_23', 'false');
			$faktura->appendChild($p_23);

			// usługi turystyki
			$p_106e_2 = $this->report->createElement('P_106E_2', 'false');
			$faktura->appendChild($p_106e_2);

			// korekta faktury
			if ($invoice->isCorrection())
			{
				$rodzajFaktury = $this->report->createElement('RodzajFaktury', 'KOREKTA');
				$faktura->appendChild($rodzajFaktury);

				$przyczynaKorekty = $this->report->createElement('PrzyczynaKorekty', $invoice->correctionReason);
				$faktura->appendChild($przyczynaKorekty);

				$nrFaKorygowanej = $this->report->createElement('NrFaKorygowanej', $invoice->correctionInvoiceNr);
				$faktura->appendChild($nrFaKorygowanej);

				$okresFaKorygowanej = $this->report->createElement('OkresFaKorygowanej', $invoice->dateIssue);
				$faktura->appendChild($okresFaKorygowanej);
			}
			else
			{
				$rodzajFaktury = $this->report->createElement('RodzajFaktury', 'VAT');
				$faktura->appendChild($rodzajFaktury);
			}

			$invoices[] = $faktura;
		}

		return $invoices;
	}

	/* ====================================================================== */
	
	/**
	 * Create invoices count
	 * 
	 * @return \DOMElement
	 */ 
	private function getInvoicesCtrl()
	{
		$fakturyCtrl = $this->report->createElement('FakturaCtrl');

		$liczbaFaktur = $this->report->createElement('LiczbaFaktur', (string)count($this->invoices));
		$fakturyCtrl->appendChild($liczbaFaktur);

		$wartoscFaktur = $this->report->createElement('WartoscFaktur', sprintf("%.2f", $this->totalNet));
		$fakturyCtrl->appendChild($wartoscFaktur);

		return $fakturyCtrl;
	}

	/* ====================================================================== */
	
	/**
	 * Create invoices rows
	 * 
	 * @return array
	 */ 
	private function getInvoicesRows()
	{
		$invoicesRows = [];

		foreach ($this->invoices as $invoice)
		{
			foreach ($invoice->getRows() as $invoiceRow)
			{
				$fakturaWiersz = $this->report->createElement('FakturaWiersz');
				$fakturaWiersz->setAttribute('typ', 'G');

				$p_2b = $this->report->createElement('P_2B', $invoice->nr);
				$fakturaWiersz->appendChild($p_2b);

				$p_7 = $this->report->createElement('P_7', htmlspecialchars($invoiceRow->description));
				$fakturaWiersz->appendChild($p_7);

				$p_8a = $this->report->createElement('P_8A', $invoiceRow->unit);
				$fakturaWiersz->appendChild($p_8a);

				$p_8b = $this->report->createElement('P_8B', $invoiceRow->quantity);
				$fakturaWiersz->appendChild($p_8b);

				$p_9a = $this->report->createElement('P_9A', $invoiceRow->unitPriceNet);
				$fakturaWiersz->appendChild($p_9a);

				$p_11 = $this->report->createElement('P_11', $invoiceRow->priceNet);
				$fakturaWiersz->appendChild($p_11);

				// skip TAX rate for reverse-charge Invoice
				if (!$invoice->isReverseCharge())
				{
					$p_12 = $this->report->createElement('P_12', $invoiceRow->taxRateName);
					$fakturaWiersz->appendChild($p_12);
				}

				$invoicesRows[] = $fakturaWiersz;
			}
		}

		return $invoicesRows;
	}

	/* ====================================================================== */
	
	/**
	 * Create invoices rows count
	 * 
	 * @return \DOMElement
	 */ 
	private function getInvoicesRowsCtrl()
	{
		$fakturaWierszCtrl = $this->report->createElement('FakturaWierszCtrl');

		$liczbaWierszyFaktur = $this->report->createElement('LiczbaWierszyFaktur', (string)count($this->invoicesRows));
		$fakturaWierszCtrl->appendChild($liczbaWierszyFaktur);

		$wartoscWierszyFaktur = $this->report->createElement('WartoscWierszyFaktur', sprintf("%.2f", $this->totalNetRows));
		$fakturaWierszCtrl->appendChild($wartoscWierszyFaktur);

		return $fakturaWierszCtrl;
	}
}