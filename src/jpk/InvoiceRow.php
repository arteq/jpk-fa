<?php
/**
 * Single invoice row position 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\JPK_FA;

class InvoiceRow
{
	public $description;
	public $quantity;
	public $unit;
	public $unitPriceNet;
	public $priceNet;
	public $taxRate;
	public $taxRateName;

	private $validTaxRates = [23, 8, 5, 0, 'zw'];

	/* ====================================================================== */
	
	/**
	 * Create new InvoiceRow
	 * 
	 * @param string $description
	 * @param double $unitPriceNet
	 * @param mixed $taxRate [23, 8, 5, 0, 'zw']
	 * @param double $quantity
	 * @param string $unit
	 */ 
	public function __construct($description, $unitPriceNet, $taxRate, $quantity = 1, $unit = 'usł.')
	{
		if (empty($description))
			throw new JpkException("Nazwa pozycji nie może być pusta");
		if (!is_numeric($unitPriceNet))
			throw new JpkException("Cena jednostkowa netto musi być liczbą");
		if ($taxRate === "")
			throw new JpkException("Nieprawidłowa stawka VAT (".implode(", ", $this->validTaxRates).")");
		if (!in_array($taxRate, $this->validTaxRates))
			throw new JpkException("Nieprawidłowa stawka VAT (".implode(", ", $this->validTaxRates).")");
		if (!is_numeric($taxRate) && $taxRate != "zw")
			throw new JpkException("Nieprawidłowa stawka VAT (".implode(", ", $this->validTaxRates).")");
		if (!is_numeric($quantity))
			throw new JpkException("Liczba jednostek musi być liczbą");

		$this->description = $description;
		$this->unitPriceNet = $unitPriceNet;
		$this->taxRateName = $taxRate;
		$this->quantity = $quantity;
		$this->unit = $unit;

		$this->priceNet = round($unitPriceNet * $quantity, 2);

		if ($taxRate == 'zw')
			$this->taxRate = 0;
		else
			$this->taxRate = $taxRate;
	}
}