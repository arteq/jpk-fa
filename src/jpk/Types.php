<?php
/**
 * Provides static data about US codes, country codes, etc...
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

 namespace ArteQ\JPK_FA;

class Types
{
	const US_CODES = __DIR__.'/../../xsd/KodyUrzedowSkarbowych_v4-0E.xsd';
	const COUNTRY_CODES = __DIR__.'/../../xsd/KodyKrajow_v4-1E.xsd';
	const NS_TYPES = 'http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/';

	/* ====================================================================== */
	
	/**
	 * Return array of US offices code => name
	 * 
	 * @return array
	 */ 
	public static function getOfficeCodes()
	{
		$result = [];

		// get file
		$dom = new \DOMDocument();
		$dom->load(self::US_CODES);

		$xp = new \DOMXPath($dom);
		$xp->registerNamespace("xsd", self::NS_TYPES);
		
		$codes = $xp->query('//xsd:schema/xsd:simpleType[@name="TKodUS"]/xsd:restriction/xsd:enumeration');
		foreach ($codes as $code)
		{
			$_code = $code->getAttribute('value');
			$_name = $xp->evaluate('string(./xsd:annotation/xsd:documentation)', $code);

			$result[$_code] = $_name;
		}

		return $result;
	}

	/* ====================================================================== */
	
	/**
	 * Return array of country codes code => name
	 * 
	 * @return array
	 */ 
	public static function getCountryCodes()
	{
		$result = [];

		// get file
		$dom = new \DOMDocument();
		$dom->load(self::COUNTRY_CODES);

		$xp = new \DOMXPath($dom);
		$xp->registerNamespace("xsd", self::NS_TYPES);
		
		$codes = $xp->query('//xsd:schema/xsd:simpleType[@name="TKodKraju"]/xsd:restriction/xsd:enumeration');
		foreach ($codes as $code)
		{
			$_code = $code->getAttribute('value');
			$_name = $xp->evaluate('string(./xsd:annotation/xsd:documentation)', $code);

			$result[$_code] = $_name;
		}

		return $result;

		
	}
}