<?php
/**
 * Invoice
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

namespace ArteQ\JPK_FA;

class Invoice
{
	const EXEMPT_REASON_FIELDS = ['P_19A', 'P_19B', 'P_19C'];

	protected $rows = [];
	protected $totalNet = 0;
	protected $totalNet_23 = 0;
	protected $totalNet_8 = 0;
	protected $totalNet_5 = 0;
	protected $totalNet_0 = 0;
	protected $totalNet_zw = 0;
	protected $totalGross = 0;
	protected $isExcempt = false;
	protected $isReverseCharge = false;
	protected $isCorrection = false;

	public $client;
	public $nr;
	public $dateIssue;
	public $dateSell;
	public $correctionReason;
	public $correctionInvoiceNr;
	public $excemptLegalBasis = [];

	/* ====================================================================== */
	
	/**
	 * Create new Invoice
	 * 
	 * @param Company $client
	 * @param string $nr
	 * @param string $dateIssue YYYY-MM-DD
	 * @param string $dateSell YYYY-MM-DD
	 */ 
	public function __construct(Company $client, $nr, $dateIssue, $dateSell)
	{
		$this->client = $client;
		$this->nr = $nr;
		$this->dateIssue = str_replace(['/', '.'], ['-', '-'], $dateIssue);
		$this->dateSell = str_replace(['/', '.'], ['-', '-'], $dateSell);

		if (empty($this->nr))
			throw new JpkException("Numer faktury nie może być pusty");
		if (preg_match('#\d{4}-\d{2}-\d{2}#', $this->dateIssue) != 1)
			throw new JpkException("Nieprawidłowy format daty wystawienia (YYYY-MM-DD)");
		if (preg_match('#\d{4}-\d{2}-\d{2}#', $this->dateSell) != 1)
			throw new JpkException("Nieprawidłowy format daty sprzedaży (YYYY-MM-DD)");
	}

	/* ====================================================================== */
	
	/**
	 * Return invoice total net value
	 * 
	 * @return double
	 */ 
	public function getTotalNet()
	{
		return round($this->totalNet, 2);
	}

	/**
	 * Return invoice net value for 23% tax rate
	 * 
	 * @return double
	 */
	public function getTotalNet_23()
	{
		return round($this->totalNet_23, 2);
	}

	/* ====================================================================== */
	
	/**
	 * Return invoice net value for 8% tax rate
	 * 
	 * @return double
	 */
	public function getTotalNet_8()
	{
		return round($this->totalNet_8, 2);
	}

	/* ====================================================================== */
	
	/**
	 * Return invoice net value for 5% tax rate
	 * 
	 * @return double
	 */
	public function getTotalNet_5()
	{
		return round($this->totalNet_5, 2);
	}

	/* ====================================================================== */
	
	/**
	 * Return invoice net value for 0% tax rate
	 * 
	 * @return double
	 */
	public function getTotalNet_0()
	{
		return round($this->totalNet_0, 2);
	}

	/* ====================================================================== */
	
	/**
	 * Return invoice net value for 'zw' tax rate
	 * 
	 * @return double
	 */
	public function getTotalNet_zw()
	{
		return round($this->totalNet_zw, 2);
	}

	/* ====================================================================== */
	
	/**
	 * Get invoice total gross value
	 * 
	 * @return double
	 */ 
	public function getTotalGross()
	{
		$gross = 0;
		$gross += round($this->totalNet_23 * 1.23, 2); 
		$gross += round($this->totalNet_8 * 1.08, 2); 
		$gross += round($this->totalNet_5 * 1.05, 2); 
		$gross += round($this->totalNet_0); 
		$gross += round($this->totalNet_zw); 

		return round($gross, 2);
	}

	/* ====================================================================== */
	
	/**
	 * Add new InvoiceRow, update invoice net values (total & tax rate)
	 * 
	 * @param InvoiceRow $row
	 */ 
	public function addRow(InvoiceRow $row)
	{
		$this->rows[] = $row;

		$this->totalNet += $row->priceNet;
		switch ($row->taxRateName)
		{
			case 'zw':
				$this->totalNet_zw += $row->priceNet;
				$this->isExcempt = true;
				break;	
				
			case 23:
				$this->totalNet_23 += $row->priceNet;
				break;

			case 8:
				$this->totalNet_8 += $row->priceNet;
				break;

			case 5:
				$this->totalNet_5 += $row->priceNet;
				break;

			case 0:
				$this->totalNet_0 += $row->priceNet;
				break;
		}
	}

	/* ====================================================================== */
	
	/**
	 * Get all InvoiceRows
	 * 
	 * @return array
	 */
	public function getRows()
	{
		return $this->rows;
	}

	/* ====================================================================== */
	
	/**
	 * Set tax reverse charge on invoice
	 * 
	 * @param bool $val
	 */ 
	public function setReverseCharge(bool $val)
	{
		$this->isReverseCharge = $val;
	}

	/* ====================================================================== */
	
	/**
	 * Get tax reverse charge flag
	 * 
	 * @return bool
	 */ 
	public function isReverseCharge()
	{
		return $this->isReverseCharge;
	}

	/* ====================================================================== */
	
	/**
	 * Get tax excempt flag, return true if there is any row with 'zw' tax rate
	 * 
	 * @return bool
	 */ 
	public function isExcempt()
	{
		return $this->isExcempt;
	}

	/* ====================================================================== */
	
	/**
	 * Get correction flag
	 * 
	 * @return bool
	 */ 
	public function isCorrection()
	{
		return $this->isCorrection;
	}

	/* ====================================================================== */
	
	/**
	 * Mark invoice as correction, set oryginal invoice Nr & reason for correction issue
	 * 
	 * @param string $correctionInvoiceNr
	 * @param string $correctionReason
	 */ 
	public function setCorrection($correctionInvoiceNr, $correctionReason)
	{
		$this->isCorrection = true;
		$this->correctionInvoiceNr = $correctionInvoiceNr;
		$this->correctionReason = $correctionReason;
	}

	/* ====================================================================== */
	
	/**
	 * Set legal basis for using excempt tax rate on invoice
	 * 
	 * @param string $reason
	 * @param string $field
	 */ 
	public function setExcemptLegalBasis($reason, $field)
	{
		if (empty($reason))
			throw new JpkException("Powód zastosowanego zwolnienia nie może być pusty");

		if (!in_array($field, self::EXEMPT_REASON_FIELDS))
			throw new JpkException("Powód zastosowanego zwolnienia musi być przypisany do jednego z pól: P_19A, P_19B, P_19C");

		$this->excemptLegalBasis[$field] = $reason;
	}
}