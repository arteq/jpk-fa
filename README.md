# Generator/validator raportów JPK_FA
Biblioteka umożliwia generowanie raportów JPK_FA dla urzędów skarbowych. 
Wygenerowany raport może być opcjonalnie sprawdzony walidatorem, który przetestuje czy:

 - struktura pliku jest zgodna ze schematem XSD
 - wiersze kontrolne zgadzają się z wprowadzonymi danymi (liczby faktur, wierszy faktur, kwoty)
 - daty wystawienia faktur odpowiadają zakresowi dat raportu
 - każda faktura ma wiersz danych oraz każdy wiersz danych należy do jakiejś faktury
 - numery faktur są unikalne
 - dla faktur korzystających ze zwolnienia VAT podano podstawę prawną tego zwolnienia


## Instalacja

```sh
    $ composer require arteq/jpk-fa
```

Opcjonalnie można przeprowadzić testy:

```sh
    $ vendor/bin/phpunit
```    

## Tworzenie raportu
Kompletny przykład kodu znajduje się w katalogu **example**:

```php
<?php

use ArteQ\JPK_FA\Types;
use ArteQ\JPK_FA\Report;
use ArteQ\JPK_FA\Company;
use ArteQ\JPK_FA\Invoice;
use ArteQ\JPK_FA\InvoiceRow;
use ArteQ\JPK_FA\Validator;

require __DIR__.'/../vendor/autoload.php';

// pobranie listy urzędów
$offices = Types::getOfficeCodes();
print_r($offices);

// pobranie listy krajów
$countries = Types::getCountryCodes();
print_r($countries);

try
{
	// tworzenie nowego raportu dla faktur wystawionych od 2019-01-01 do 2019-02-02
	// dla Urzędu Skarbowego o symbolu 0202
	$report = new Report('2019-01-01', '2019-02-02', '0202');

	// dane firmy przygotowującej raport
	$company = new Company('ACME Ltd', '666-555-44-33');
	$company->set('REGON', '123123123');
	$company->set('Wojewodztwo', 'Mazowieckie');
	$company->set('Powiat', 'Warszawa');
	$company->set('Gmina', 'Warszawa');
	$company->set('Ulica', 'Wiejska');
	$company->set('NrDomu', '12');
	$company->set('NrLokalu', '34');
	$company->set('Miejscowosc', 'Warszawa');
	$company->set('KodPocztowy', '01-007');
	$company->set('Poczta', 'Warszawa');

	$report->setCompany($company);

	// dane klienta
	$client = new Company('ACME Ltd', '111 22 33 444', 'Pełen adres');

	// dane pierwszej faktury z jedną pozycją
	$invoice1 = new Invoice($client, '2019/01/01', '2019-01-02', '2019-01-01');
	$invoice1->addRow(new InvoiceRow('pozycja 1', 100, 23 ));
	$report->addInvoice($invoice1);

	// dane drugiej faktury z wieloma pozycjami
	$invoice2 = new Invoice($client, '2019/01/02', '2019-01-04', '2019-01-04');
	$invoice2->addRow(new InvoiceRow('pozycja 11', 111.11, 23, 1));
	$invoice2->addRow(new InvoiceRow('pozycja 22', 222.22, 8, 2));
	$invoice2->addRow(new InvoiceRow('pozycja 33', 333.33, 5, 3));
	$invoice2->addRow(new InvoiceRow('pozycja 44', 444.44, 0, 4));
	$invoice2->addRow(new InvoiceRow('pozycja 55', 555.55, 'zw', 5));
	$invoice2->setExcemptLegalBasis('podstawa prawna', 'P_19A');
	$report->addInvoice($invoice2);

	// generowanie i wyświetlenie raportu
	$reportXml = $report->generate();
	echo $reportXml;

	// sprawdzenie poprawności
	$validator = new Validator($reportXml);
	if ($validator->isValid())
	{
		echo "\nRAPORT POPRAWNY\n";
	}
	else
	{
		echo "\nRAPORT NIEPOPRAWNY\n";
		$errors = $validator->getErrors();
		print_r($errors);
	}
}
catch (\Exception $e)
{
	echo "Błąd: ".$e->getMessage();
}
```

## Walidacja
Poprawność raportu można sprawdzić przy pomocy klasy **Validator**, która w konstruktorze przyjmuje string z treścią pliku XML. Do sprawdzenia poprawności służą dwie metody: **isValid()** oraz **getErrors()**.

```php
$reportXml = file_get_contents('raport.xml');
$validator = new Validator($reportXml);

if ($validator->isValid())
	echo "raport OK";
else
	echo "raport BŁĘDNY";
			
$errors = $validator->getErrors();
var_dump($errors);
```	

## Credits
Biblioteka jest wzorowana na: https://github.com/redgda/jpk