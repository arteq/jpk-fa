<?php
/**
 * 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\JPK_FA\InvoiceRow;
use ArteQ\JPK_FA\JpkException;

class InvoiceRowTest extends TestCase
{
	public function testCanCreateDefault()
	{
		$row = new InvoiceRow('Opis', 123.45, 23);
		$this->assertEquals(1, $row->quantity);
		$this->assertEquals('usł.', $row->unit);
	}

	/* ====================================================================== */
	
	/**
	 * @dataProvider dataMissingParams
	 */ 
	public function testCantCreateWithMissingParams($description, $unitPriceNet, $taxRate)
	{
		$this->expectException(JpkException::class);

		$row = new InvoiceRow($description, $unitPriceNet, $taxRate);
	}

	/* ====================================================================== */
	
	/**
	 * @dataProvider dataPrices
	 */
	public function testNetPrice($unitPriceNet, $quantity, $expectedPrice)
	{
		$row = new InvoiceRow('Opis', $unitPriceNet, 23, $quantity);
		$this->assertEquals($row->priceNet, $expectedPrice, 0.009);
	}

	/* ====================================================================== */
	
	/**
	 * @dataProvider dataTaxValid
	 */ 
	public function testTaxRateValid($tax)
	{
		$row = new InvoiceRow('Opis', 123.45, $tax);
		$this->assertEquals($tax, $row->taxRateName);
	}

	/* ====================================================================== */
	
	/**
	 * @dataProvider dataTaxInvalid
	 */ 
	public function testTaxRateInvalid($tax)
	{
		$this->expectException(JpkException::class);

		$row = new InvoiceRow('Opis', 123.45, $tax);
	}

	/* ====================================================================== */
	/* DATA PROVIDERS ======================================================= */
	/* ====================================================================== */
	
	public function dataTaxValid()
	{
		return [
			[23],
			[8],
			[5],
			[0],
			[0.0],
			['zw'],
		];
	}

	/* ====================================================================== */
	
	public function dataTaxInvalid()
	{
		return [
			[15],
			[0.1],
			['zw.'],
			['zwolniony'],
			[''],
		];
	}

	/* ====================================================================== */
	
	/**
	 * @return array [$description, $unitPriceNet, $taxRate]
	 */ 
	public function dataMissingParams()
	{
		return [
			['', 123.45, 23],
			['Opis', null, 23],
			['Opis', 123.45, null],
		];
	}

	/* ====================================================================== */
	
	/**
	 * @return array [$unitPriceNet, $quantity, $expectedPrice]
	 */ 
	public function dataPrices()
	{
		return [
			[123.45, 1, 123.45],
			[123.45, 2, 246.90],
			[123.45, 3, 370.35],
			[0.01, 1, 0.01],
			[0.01, 20, 0.2],
			[0.05, 1, 0.05],
			[0.05, 2.22, 0.11],
		];
	}
}