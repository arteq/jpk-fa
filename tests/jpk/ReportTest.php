<?php
/**
 * 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\JPK_FA\Report;
use ArteQ\JPK_FA\Company;
use ArteQ\JPK_FA\Invoice;
use ArteQ\JPK_FA\InvoiceRow;
use ArteQ\JPK_FA\Validator;

class ReportTest extends TestCase
{
	private $report;

	/* ====================================================================== */
	
	public function setUp()
	{
		$report = new Report('2019-01-01', '2019-02-02', '0202');

		$company = new Company('ACME Ltd', '677-208-20-82');
		$company->set('REGON', '351570553');
		$company->set('Wojewodztwo', 'Mazowieckie');
		$company->set('Powiat', 'Warszawa');
		$company->set('Gmina', 'Warszawa');
		$company->set('Ulica', 'Wiejska');
		$company->set('NrDomu', '12');
		$company->set('NrLokalu', '34');
		$company->set('Miejscowosc', 'Warszawa');
		$company->set('KodPocztowy', '01-007');
		$company->set('Poczta', 'Warszawa');

		$report->setCompany($company);
		
		$client = new Company('ACME Ltd', '677 20 82 082', 'Pełen adres');
		$invoice1 = new Invoice($client, '2019/01/01', '2019-01-02', '2019-01-01');
		$invoice1->addRow(new InvoiceRow('pozycja 1', 100, 23 ));
		$report->addInvoice($invoice1);

		$invoice2 = new Invoice($client, '2019/01/02', '2019-01-04', '2019-01-04');
		$invoice2->addRow(new InvoiceRow('pozycja 11', 111.11, 23, 1));
		$invoice2->addRow(new InvoiceRow('pozycja 22', 222.22, 8, 2));
		$invoice2->addRow(new InvoiceRow('pozycja 33', 333.33, 5, 3));
		$invoice2->addRow(new InvoiceRow('pozycja 44', 444.44, 0, 4));
		$invoice2->addRow(new InvoiceRow('pozycja 55', 555.55, 'zw', 5));
		$invoice2->setExcemptLegalBasis('podstawa prawna', 'P_19A');
		$report->addInvoice($invoice2);

		$this->report = $report;
	}

	/* ====================================================================== */
	
	public function testCanGenerate()
	{
		$reportXml = $this->report->generate();
		
		$this->assertTrue(is_string($reportXml));
		$this->assertNotContains('<RodzajFaktury>KOREKTA', $reportXml);
		$this->assertNotContains('<NrFaKorygowanej>', $reportXml);
		$this->assertNotContains('<OkresFaKorygowanej>', $reportXml);
		$this->assertNotContains('<PrzyczynaKorekty>', $reportXml);
	}

	/* ====================================================================== */
	
	public function testCanAddInvoice()
	{
		$company = new Company('Testowa', '2082677208', 'Inny adres');
		$invoice = new Invoice($company, '2019/02/02', '2019-02-02', '2019-02-01');

		$row = new InvoiceRow('pozycja 2', 100, 8 );
		$invoice->addRow($row);

		$this->report->addInvoice($invoice);

		$reportXml = $this->report->generate();
		$validator = new Validator($reportXml);

		// echo $reportXml;
		$this->assertTrue($validator->isValid() );
		$this->assertEmpty($validator->getErrors() );
		$this->assertFalse($invoice->isExcempt());
	}

	/* ====================================================================== */
	
	public function testIsValidSchema()
	{
		$reportXml = $this->report->generate();
		$validator = new Validator($reportXml);
		
		$this->assertTrue($validator->isValid() );
		$this->assertEmpty($validator->getErrors() );

		$reportXml = file_get_contents(__DIR__.'/../fixtures/valid.xml');
		$validator = new Validator($reportXml);
		
		$this->assertTrue($validator->isValid() );
		$this->assertEmpty($validator->getErrors() );
	}

	/* ====================================================================== */
	
	public function testWrongDates()
	{
		$reportXml = file_get_contents(__DIR__.'/../fixtures/wrong_dates.xml');
		$validator = new Validator($reportXml);
		
		$errors = $validator->getErrors();
		// var_dump($errors);
		$this->assertFalse($validator->isValid() );
		$this->assertEquals(4, count($errors));
	}

	/* ====================================================================== */
	
	public function testMissingInvoiceRow()
	{
		$reportXml = file_get_contents(__DIR__.'/../fixtures/wrong_invoice_row.xml');
		$validator = new Validator($reportXml);
		
		$errors = $validator->getErrors();
		// var_dump($errors);
		$this->assertFalse($validator->isValid() );
		$this->assertEquals(4, count($errors));
	}

	/* ====================================================================== */
	
	public function testUniqueInvoiceNr()
	{
		$reportXml = file_get_contents(__DIR__.'/../fixtures/wrong_invoice_nr.xml');
		$validator = new Validator($reportXml);
		
		$errors = $validator->getErrors();
		// var_dump($errors);
		$this->assertFalse($validator->isValid() );
		$this->assertEquals(1, count($errors));
	}

	/* ====================================================================== */
	
	public function testInvalidInvoiceNet()
	{
		$reportXml = file_get_contents(__DIR__.'/../fixtures/wrong_invoice_net.xml');
		$validator = new Validator($reportXml);
		
		$errors = $validator->getErrors();
		// var_dump($errors);
		$this->assertFalse($validator->isValid() );
		$this->assertEquals(2, count($errors));
	}

	/* ====================================================================== */
	
	public function testInvoiceCorrection()
	{
		$company = new Company('Testowa', '2082677208', 'Inny adres');
		$invoice = new Invoice($company, 'KOR 2019/01/01', '2019-01-02', '2019-01-01');

		$row = new InvoiceRow('pozycja 1', 100, 23 );
		$invoice->addRow($row);
		$invoice->setCorrection('2019/01/01', 'przyczyna');

		$this->report->addInvoice($invoice);
		$reportXml = $this->report->generate();

		$this->assertContains('<RodzajFaktury>KOREKTA</RodzajFaktury>', $reportXml);
		$this->assertContains('<NrFaKorygowanej>2019/01/01</NrFaKorygowanej>', $reportXml);
		$this->assertContains('<OkresFaKorygowanej>2019-01-02</OkresFaKorygowanej>', $reportXml);
		$this->assertContains('<PrzyczynaKorekty>przyczyna</PrzyczynaKorekty>', $reportXml);
	}

	/* ====================================================================== */
	
	public function testNonUEClient()
	{
		$company = new Company('Testowa', '2082677208', 'Inny adres', 'US');
		$invoice = new Invoice($company, 'KOR 2019/01/01', '2019-01-02', '2019-01-01');

		$row = new InvoiceRow('pozycja 1', 100, 23 );
		$invoice->addRow($row);

		$this->report->addInvoice($invoice);
		$reportXml = $this->report->generate();

		$this->assertContains('KodKraju>PL', $reportXml);
		$this->assertNotContains('KodKraju>US', $reportXml);
	}

	/* ====================================================================== */
	
	public function testExemptLegalBasis()
	{
		$company = new Company('Testowa', '2082677208', 'Inny adres', 'PL');
		$invoice = new Invoice($company, '2019/01/99', '2019-01-02', '2019-01-01');

		$row = new InvoiceRow('pozycja 1', 100, 'zw' );
		$invoice->addRow($row);
		$invoice->setExcemptLegalBasis('podstawa prawna A', 'P_19A');
		$invoice->setExcemptLegalBasis('podstawa prawna B', 'P_19B');
		$invoice->setExcemptLegalBasis('podstawa prawna C', 'P_19C');

		$this->report->addInvoice($invoice);
		$reportXml = $this->report->generate();

		$this->assertContains('P_19A>podstawa prawna A', $reportXml);
		$this->assertContains('P_19B>podstawa prawna B', $reportXml);
		$this->assertContains('P_19C>podstawa prawna C', $reportXml);

		$validator = new Validator($reportXml);
		$this->assertTrue($validator->isValid() );
	}

	/* ====================================================================== */
	
	public function testMissingExemptLegalBasis()
	{
		$report = new Report('2019-01-01', '2019-02-02', '0202');

		$company = new Company('ACME Ltd', '677-208-20-82');
		$company->set('REGON', '351570553');
		$company->set('Wojewodztwo', 'Mazowieckie');
		$company->set('Powiat', 'Warszawa');
		$company->set('Gmina', 'Warszawa');
		$company->set('Ulica', 'Wiejska');
		$company->set('NrDomu', '12');
		$company->set('NrLokalu', '34');
		$company->set('Miejscowosc', 'Warszawa');
		$company->set('KodPocztowy', '01-007');
		$company->set('Poczta', 'Warszawa');

		$report->setCompany($company);
		
		$client = new Company('ACME Ltd', '677 20 82 082', 'Pełen adres');
		$invoice1 = new Invoice($client, '2019/01/01', '2019-01-02', '2019-01-01');
		$invoice1->addRow(new InvoiceRow('pozycja 1', 100, 'zw' ));
		$report->addInvoice($invoice1);

		$reportXml = $report->generate();
		$validator = new Validator($reportXml);

		$errors = $validator->getErrors();
		// var_dump($errors);
		$this->assertFalse($validator->isValid() );
		$this->assertEquals(1, count($errors));
	}
}