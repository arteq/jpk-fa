<?php
/**
 * 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\JPK_FA\Company;
use ArteQ\JPK_FA\JpkException;

class CompanyTest extends TestCase
{
	private $company;

	/* ====================================================================== */
	
	public function setUp()
	{
		$this->company = new Company('ACME Ltd', '123-123-1231');
	}

	/* ====================================================================== */
	
	public function testCanCreateMinimal()
	{
		$this->assertEquals('ACME Ltd', $this->company->get('PelnaNazwa'));
		$this->assertEquals('PL', $this->company->get('KodKraju'));
		$this->assertEquals('1231231231', $this->company->get('NIP'));
		$this->assertEmpty($this->company->get('Adres'));
	}

	/* ====================================================================== */
	
	public function testCanSetAdditionalFields()
	{
		$this->company->set('Wojewodztwo', 'Mazowieckie');
		$this->company->set('Powiat', 'Warszawski');
		$this->company->set('Ulica', 'Dziwna');
		$this->company->set('NrDomu', '123');

		$this->assertEquals('Mazowieckie', $this->company->get('Wojewodztwo') );
		$this->assertEquals('Warszawski', $this->company->get('Powiat') );
		$this->assertEquals('Dziwna', $this->company->get('Ulica') );
		$this->assertEquals('123', $this->company->get('NrDomu') );
	}

	/* ====================================================================== */
	
	public function testCanCreateWithAddressSingle()
	{
		$company = new Company('Wrong', '111-222-333-4', "Full\nAddress with newlines\n9/4");
		$this->assertEquals('Full Address with newlines 9/4', $company->get('Adres'));

		$company = new Company('Wrong', '111-222-333-4', "Full\rAddress\nwith newlines\r\n9/4");
		$this->assertEquals('Full Address with newlines 9/4', $company->get('Adres'));
	}

	/* ====================================================================== */
	
	public function testCanSetAddressSingle()
	{
		$company = new Company('Wrong', '111-222-333-4');
		$company->set('Adres', "Full\nAddress with newlines\n9/4");
		$this->assertEquals('Full Address with newlines 9/4', $company->get('Adres'));
	}

	/* ====================================================================== */
	
	public function testCanSetAddressMultiple()
	{
		$this->company->set('Miejscowość', 'Miejscowość ');
		$this->company->set('Poczta', ' Poczta');
		$this->company->set('KodPocztowy', ' 01-234 ');
		$this->company->set('Ulica', 'Dziwna');
		$this->company->set('NrDomu', '123  ');
		$this->company->set('NrLokalu', '  101C');

		$this->assertEquals('Dziwna 123/101C, 01-234 Poczta', $this->company->get('Adres'));
	}

	/* ====================================================================== */
	
	public function testCantCreateWithWrongNIP()
	{
		$this->expectException(JpkException::class);
		$company = new Company('ACME', '123-123');
	}

	/* ====================================================================== */
	
	public function testCanCreateWithForeignNIP()
	{
		$company = new Company('ACME', '123-123', 'Foreign address', 'UK');
		$this->assertEquals('123-123', $company->get('NIP'));
	}

	/* ====================================================================== */
	
	public function testCantCreateWithEmptyName()
	{
		$this->expectException(JpkException::class);
		$company = new Company('', '123-123-123-3');
	}

	/* ====================================================================== */
	
	/**
	 * @dataProvider dataNIP
	 */ 
	public function testIsSanitizedNIP($nip)
	{
		$this->company->set('NIP', $nip);
		$this->assertEquals(1, preg_match('/^\d{10}$/', $this->company->get('NIP')));
	}

	/* ====================================================================== */

	/**
	 * @dataProvider dataREGON
	 */ 
	public function testIsSanitizedREGON($regon)
	{
		$this->company->set('REGON', $regon);
		$this->assertEquals(1, preg_match('/^\d{9}$/', $this->company->get('REGON')));
	}

	/* ====================================================================== */
	
	/**
	 * Provide NIP numbers with extra chars that should be stripped
	 * 
	 * @return array
	 */ 
	public function dataNIP()
	{
		return [
			['1231231231'],
			['123-123-1231'],
			[' 123 123 1231'],
			['aa 123-123 bb 123 1 '],
		];
	}

	/* ====================================================================== */
	
	/**
	 * Provide REGON numbers with extra chars that should be stripped
	 * 
	 * @return array
	 */ 
	public function dataREGON()
	{
		return [
			['351570553'],
			['35-157-0553'],
			[' 35 157 0553'],
			['aa 35-157 bb 055 3 '],
		];
	}
}