<?php
/**
 * 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\JPK_FA\Invoice;
use ArteQ\JPK_FA\InvoiceRow;
use ArteQ\JPK_FA\Company;
use ArteQ\JPK_FA\JpkException;

class InvoiceTest extends TestCase
{
	private $invoice;

	/* ====================================================================== */
	
	public function setUp()
	{
		$company = new Company('ACME Ltd', '6772082082');
		$invoice = new Invoice($company, '2019/01/01', '2019-01-02', '2019-01-01');

		$row = new InvoiceRow('pozycja 1', 100, 23 );
		$invoice->addRow($row);

		$row = new InvoiceRow('pozycja 2', 222.22, 23, 5);
		$invoice->addRow($row);

		$row = new InvoiceRow('pozycja 3', 300, 'zw');
		$invoice->addRow($row);
		$invoice->setExcemptLegalBasis('podstawa prawna', 'P_19A');

		$this->invoice = $invoice;
	}

	/* ====================================================================== */
	
	public function testTotalNet()
	{
		$totalNet = $this->invoice->getTotalNet();
		$this->assertEquals(1511.10, $totalNet, '', 0.009);
	}

	/* ====================================================================== */
	
	public function testTotalGross()
	{
		$totalGross = $this->invoice->getTotalGross();
		$this->assertEquals(1789.65, $totalGross, '', 0.009);
	}

	/* ====================================================================== */
	
	public function testTotalByTaxRate()
	{
		$this->assertEquals(1211.1, $this->invoice->getTotalNet_23(), 0.009);
		$this->assertEquals(0, $this->invoice->getTotalNet_8(), 0.009);
		$this->assertEquals(0, $this->invoice->getTotalNet_5(), 0.009);
		$this->assertEquals(0, $this->invoice->getTotalNet_0(), 0.009);
		$this->assertEquals(300, $this->invoice->getTotalNet_zw(), 0.009);
	}

	/* ====================================================================== */

	public function testCanCreateWithOtherDateSeparator()
	{
		$company = new Company('ACME Ltd', '6772082082');
		$invoice = new Invoice($company, '2019/01/01', '2019/01/02', '2019.01.01');

		$this->assertEquals('2019-01-02', $invoice->dateIssue);
		$this->assertEquals('2019-01-01', $invoice->dateSell);
	}

	/* ====================================================================== */
	
	/**
	 * @dataProvider wrongDates
	 */ 
	public function testCantCreateWithWrongDate($dateIssue, $dateSell)
	{
		$this->expectException(JpkException::class);
		
		$company = new Company('ACME Ltd', '6772082082');
		$invoice = new Invoice($company, '2019/01/01', $dateIssue, $dateSell);
	}

	/* ====================================================================== */
	
	public function testIsExempt()
	{
		$this->assertTrue($this->invoice->isExcempt());
	}

	/* ====================================================================== */
	
	public function wrongDates()
	{
		return [
			['2019-01-01', '2019-02 02'],
			['2019-01 01', '2019-02/02'],
			['20190101', '2019-02-02'],
			['', '2019-02-02'],
			['2019-02-02', ''],
			['2019-02-02', 'wrong'],
		];
	}
}
