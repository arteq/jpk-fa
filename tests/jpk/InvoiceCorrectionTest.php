<?php
/**
 * 
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

use PHPUnit\Framework\TestCase;
use ArteQ\JPK_FA\Invoice;
use ArteQ\JPK_FA\InvoiceRow;
use ArteQ\JPK_FA\Company;
use ArteQ\JPK_FA\JpkException;

class InvoiceCorrectionTest extends TestCase
{
	private $invoice;

	/* ====================================================================== */
	
	public function setUp()
	{
		$company = new Company('ACME Ltd', '6772082082');
		$invoice = new Invoice($company, 'KOR 2019/01/01', '2019-01-02', '2019-01-01');

		$row = new InvoiceRow('pozycja 1', 100, 23 );
		$invoice->addRow($row);

		$invoice->setCorrection('2019/01/01', 'przyczyna wystawienia faktury korygującej');

		$this->invoice = $invoice;
	}

	/* ====================================================================== */
	
	public function testIsCorrection()
	{
		$this->assertTrue($this->invoice->isCorrection());
		$this->assertNotEmpty($this->invoice->correctionReason);
		$this->assertNotEmpty($this->invoice->correctionInvoiceNr);
	}
}