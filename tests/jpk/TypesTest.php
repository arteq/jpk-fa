<?php
/**
 * Test static types
 *
 * @author 		Artur Grącki <arteq@arteq.org>
 * @copyright 	Copyright (c) 2019. All rights reserved.
 */

 use PHPUnit\Framework\TestCase;
 use ArteQ\JPK_FA\Types;

 class TypesTest extends TestCase
 {
 	public function testCanGetUScodes()
 	{
 		$codes = Types::getOfficeCodes();

 		$this->assertNotEmpty($codes);

 		$this->assertArrayHasKey('0203', $codes);
 		$this->assertEquals('URZĄD SKARBOWY W BYSTRZYCY KŁODZKIEJ', $codes['0203']);

 		$this->assertArrayHasKey('3271', $codes);
 		$this->assertEquals('ZACHODNIOPOMORSKI URZĄD SKARBOWY W SZCZECINIE', $codes['3271']);

 		$this->assertCount(400, $codes);
 	}

 	/* ====================================================================== */
 	
 	public function testCanGetCountryCodes()
 	{
 		$codes = Types::getCountryCodes();

 		$this->assertNotEmpty($codes);

 		$this->assertArrayHasKey('AF', $codes);
 		$this->assertEquals('AFGANISTAN', $codes['AF']);

 		$this->assertArrayHasKey('PL', $codes);
 		$this->assertEquals('POLSKA', $codes['PL']);

 		$this->assertCount(249, $codes);
 	}
 }